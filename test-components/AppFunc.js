export const AppFunc  = () => {
  const [input, setInput] = useState({
    username: "",
    password: "",
  });
  const handleOnChange = (e) => {
    setInput({ ...input, [e.target.id]: e.target.value });
  };
  const handleOnSubmit = (e) => {
    e.preventDefault();
    setInput({ username: "", password: "" });
  };
  
  return (
    <div>
      <form onSubmit={handleOnSubmit}>
        <LabelInput
          title="Username"
          type="text"
          id="username"
          value={input.username}
          onChange={handleOnChange}
        />
        <LabelInput
          title="Password"
          type="password"
          id="password"
          value={input.password}
          onChange={handleOnChange}
        />
        <Button label="submit" />
      </form>
    </div>
  );
};



