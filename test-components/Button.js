import React from "react";

export const Button = ({ label }) => {
  return <button type="submit">{label}</button>;
};

export default Button;
