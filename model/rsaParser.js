const babelParser = require("@babel/parser");

const reduceJsonAst = (previousNode, currentNode) => {
  let nodeHolder;
  if (currentNode.type === "JSXElement") {
    nodeHolder = {
      name: currentNode.openingElement.name.name,
      children: [],
    };
    previousNode.push(nodeHolder);
  }
  if ("children" in currentNode) {
    currentNode.children.forEach((node) =>
      reduceJsonAst(nodeHolder.children, node)
    );
  }
  return previousNode;
};

const getJsonTree = (content) => {
  const rawJsonAst = babelParser.parse(content, {
    sourceType: "module",
    plugins: ["jsx", "classProperties"],
  });

  const exportComponent = rawJsonAst.program.body.find(
    (node) =>
      node.type === "ExportNamedDeclaration" ||
      node.type === "ExportDefaultDeclaration"
  );

  let initialNode, componentName;

  // Exported Named Declaration
  if (exportComponent.type === "ExportNamedDeclaration") {
    // Function Component
    if (exportComponent.declaration.type === "VariableDeclaration") {
      initialNode = exportComponent.declaration.declarations[0].init.body.body.find(
        (node) => node.type === "ReturnStatement"
      ).argument;
      componentName = exportComponent.declaration.declarations[0].id.name;
    }
    // Class Component
    else if (exportComponent.declaration.type === "ClassDeclaration") {
      initialNode = exportComponent.declaration.body.body
        .find((node) => node.key.name === "render")
        .body.body.find((node) => node.type === "ReturnStatement").argument;
      componentName = exportComponent.declaration.id.name;
    }
  }

  // Exported Default Declaration
  else {
    // Function Component
    if (exportComponent.declaration.type === "AssignmentExpression") {
      initialNode = exportComponent.declaration.right.body.body.find(
        (node) => node.type === "ReturnStatement"
      ).argument;
      componentName = exportComponent.declaration.left.name;
    } else if (exportComponent.declaration.type === "FunctionDeclaration") {
      initialNode = exportComponent.declaration.body.body.find(
        (node) => node.type === "ReturnStatement"
      ).argument;
      componentName = exportComponent.declaration.id.name;
    }
    // Class Component
    else if (exportComponent.declaration.type === "ClassDeclaration") {
      initialNode = exportComponent.declaration.body.body
        .find((node) => node.key.name === "render")
        .body.body.find((node) => node.type === "ReturnStatement").argument;
      componentName = exportComponent.declaration.id.name;
    }
  }

  return { componentName, tree: reduceJsonAst([], initialNode) };
};

module.exports = {
  getJsonTree,
};
