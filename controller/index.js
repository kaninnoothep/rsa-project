const { getJsonTree } = require("../model/rsaParser");
const express = require("express");
const app = express();
const upload = require("express-fileupload");

app.use(upload());

app.post("/upload", (req, res) => {
  const content = req.files.myFile.data.toString("utf8");
  const result = getJsonTree(content);
  res.header("Access-Control-Allow-Origin", "*");
  res.json(result);
});

app.listen(3000, function () {
  console.log("Your node js server is running");
});
